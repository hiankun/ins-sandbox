# function call: x=Nav_eq(x,u,dt)
#
# Function that implements the navigation equations of an INS.
#
# Inputs:
# x         Current navigation state [position (NED), velocity (NED), attitude (Quaternion)]
# u         Measured inertial quantities [Specific force (m/s^2), Angular velocity (rad/s)]
# dt        Sampling period (s)
#
# Output:
# x         Updated navigation state [position (NED), velocity (NED), attitude (Quaternion)]
#
# Edit: Isaac Skog (skog@kth.se), 2016-09-06
#
# python: thk, 2023-08-23
#

import numpy as np
from tools.q2dcm import q2dcm
#from tools.gravity import gravity

#def Nav_eq(x,u,dt):
def Nav_eq(x,u,dt,g_t):

    # Position and velocity
    
    # Gravity vector (This should depend on the current location, but
    # since moving in a small area it is approximatly constant).
    #g_t=gravity(59,0)
    #g_t=gravity(55,0)
    
    f_t = q2dcm(x[6:10])@u[0:3]
    
    # Subtract gravity, to obtain accelerations in tangent plane coordinates
    acc_t = f_t - g_t
    
    # state space model matrices
    A = np.eye(6)
    A[0,3] = dt
    A[1,4] = dt
    A[2,5] = dt
    
    B = np.vstack(((dt**2)/2*np.eye(3), dt*np.eye(3)))
    
    # Position and velocity update
    x[0:6] = A@x[0:6] + B@acc_t
    
    
    # Attitude Quaternion
    # Quaternion algorithm according to Farrel, pp 49.
    
    w_tb=u[3:6]
    
    P = w_tb[0]*dt
    Q = w_tb[1]*dt
    R = w_tb[2]*dt
    
    OMEGA = np.zeros((4,4))
    OMEGA[0,0:4] = 0.5*np.hstack(( 0, R,-Q, P))
    OMEGA[1,0:4] = 0.5*np.hstack((-R, 0, P, Q))
    OMEGA[2,0:4] = 0.5*np.hstack(( Q,-P, 0, R))
    OMEGA[3,0:4] = 0.5*np.hstack((-P,-Q,-R, 0))
    
    v = np.linalg.norm(w_tb)*dt
    
    if v != 0:
        x[6:10] = (np.cos(v/2)*np.eye(4)+2/v*np.sin(v/2)*OMEGA )@x[6:10]

    return x
