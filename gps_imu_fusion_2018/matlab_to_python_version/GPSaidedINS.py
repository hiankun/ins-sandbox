#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# function call: out_data=GPSaidedINS(in_data,settings)
#
# This function integrates GNSS and IMU data. The data fusion
# is based upon a loosely-coupled feedback GNSS-aided INS approach.
#
# Input struct:
# in_data.GNSS.pos_ned      GNSS-receiver position estimates in NED
#                           coordinates [m]
# in_data.GNSS.t            Time of GNSS measurements [s]
# (in_data.GNSS.HDOP        GNSS Horizontal Dilution of Precision [-]) 
# (in_data.GNSS.VDOP        GNSS Vertical Dilution of Precision [-]) 
# in_data.IMU.acc           Accelerometer measurements [m/s^2]
# in_data.IMU.gyro          Gyroscope measurements [rad/s]
# in_data.IMU.t             Time of IMU measurements [s]
#
# Output struct:
# out_data.x_h              Estimated navigation state vector [position; velocity; attitude]
# out_data.delta_u_h        Estimated IMU biases [accelerometers; gyroscopes]
# out_data.diag_P           Diagonal elements of the Kalman filter state
#                           covariance matrix.
#
#
# Edit: Isaac Skog (skog@kth.se), 2016-09-06
# Revised: Bo Bernhardsson, 2018-01-01
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import numpy as np
from dotmap import DotMap
from tools.Rt2b import Rt2b
from tools.get_Rb2p import get_Rb2p
from tools.dcm2q import dcm2q
from tools.q2dcm import q2dcm
from Nav_eq import Nav_eq

def GPSaidedINS(in_data, settings):

    # Copy data to variables with shorter name
    u = np.vstack((in_data.IMU.acc, in_data.IMU.gyro))
    t = in_data.IMU.t #FIX: shape should be (29849, 1)??
    
    # Initialization
    # Initialize the navigation state
    x_h = init_navigation_state(u, settings)
    
    # Initialize the sensor bias estimate
    delta_u_h = np.zeros((6,1))
    
    # Initialize the Kalman filter
    P, Q1, Q2, _, _ = init_filter(settings)
    
    # Allocate memory for the output data
    #N = u.shape[1]
    N = 29810 #FIX: exception when GNSS data out of index
    out_data = DotMap()
    out_data.x_h = np.zeros((10, N))
    out_data.x_h[:, 0] = x_h.squeeze()
    out_data.diag_P = np.zeros((15, N))
    out_data.diag_P[:, 0] = np.diag(P)
    out_data.delta_u_h = np.zeros((6,N))
    
    # Information fusion
    ctr_gnss_data = 0
    ctr_speed_data = 0
    
    for k in range(1, N):
        if k%1000 == 0: #FIX
            print(f'step {k}/{N}')
        # Sampling period
        Ts = t[k] - t[k-1]
        
        # Calibrate the sensor measurements using current sensor bias estimate.
        u_h = u[:,k:k+1] + delta_u_h
        
        # Update the INS navigation state
        x_h = Nav_eq(x_h, u_h, Ts, settings.gravity)
        
        # Get state space model matrices
        F, G = state_space_model(x_h, u_h, Ts)
        
        # Time update of the Kalman filter state covariance.
        O = np.zeros((6,6)) #Algorithm 1, LN13: O_3x3 should be O_6x6
        diag_Q1Q2 = np.block([
            [Q1, O],
            [O, Q2],
        ])
        
        P = F@P@F.T + G@diag_Q1Q2@G.T #15x15
        
        # Defualt measurement observation matrix  and measurement covariance
        # matrix
        
        y1 = in_data.GNSS.pos_ned[:, ctr_gnss_data:ctr_gnss_data+1]
        y2 = in_data.SPEEDOMETER.speed[:, ctr_speed_data:ctr_speed_data+1]
        y3 = np.zeros((2,1))
        y = np.block([
            [y1],
            [y2],
            [y3],
        ])
          
        Rn2p = get_Rb2p()@q2dcm(x_h[6:10]).T
        # eq(22)
        H = np.block([
            [np.eye(3), np.zeros((3,12))],
            [np.zeros((3,3)), Rn2p, np.zeros((3,9))],
        ])
        
        R = np.block([
        [settings.sigma_gps**2*np.eye(3), np.zeros((3,3))],
        [np.zeros((1,3)), settings.sigma_speed**2*np.eye(1), np.zeros((1,2))],
        [np.zeros((2,4)), settings.sigma_non_holonomic**2*np.eye(2)],
        ])

    #---> Hint: error standard deviations are in settings.sigma_speed and settings.sigma_non_holonomic
        
        ind = np.zeros(6).astype(bool) # index vector, describing available measurements     
        # Check if GNSS measurement is available
        if t[k] == in_data.GNSS.t[ctr_gnss_data]:
            if (t[k]<settings.outagestart
                or t[k]>settings.outagestop
                or settings.gnss_outage != 'on'): #TODO: wtf, why not =='off'?
                ind[0:3] = True
            # Update GNSS data counter
            ctr_gnss_data = min(ctr_gnss_data+1, len(in_data.GNSS.t))

        if t[k]==in_data.SPEEDOMETER.t[ctr_speed_data]:
            if settings.speed_aiding == 'on':
                ind[3] = True
            # Update SPEEDOMETER data counter
            ctr_speed_data = min(ctr_speed_data+1, len(in_data.SPEEDOMETER.t))

        if settings.non_holonomic == 'on':
            ind[4:6] = True
    
        ind = np.expand_dims(ind,axis=1)
        H = np.where(ind, H, 0) #(6,15)
        y = np.where(ind, y, 0) #(6,1)
        R = np.where(ind@ind.T, R, 0) #(6,6)
        
        # Calculate the Kalman filter gain.
        # https://dsp.stackexchange.com/questions/33837/
        # why-is-this-matrix-invertible-in-the-kalman-gain
        K = (P@H.T)@np.linalg.pinv(H@P@H.T+R) #15x6
        
        # Update the perturbation state estimate.
        # Algorithm 1, LN24: the final -O_3x1 part seems wrong
        z = np.block([
            [np.zeros((9,1))],
            [delta_u_h],
            ]) + K@(y-H[:,0:6]@x_h[0:6]) #(15,1)
        
        # Correct the navigation states using current perturbation estimates.
        x_h[0:6] = x_h[0:6] + z[0:6] #x:(10,1)
        x_h[6:10] = Gamma(x_h[6:10], z[6:9])
        delta_u_h = z[9:15] #(6,1)

        # Update the Kalman filter state covariance.
        P = (np.eye(15)-K@H)@P #(15,15)
       
        # Save the data to the output data structure
        out_data.x_h[:,k:k+1] = x_h
        out_data.diag_P[:,k] = np.diag(P)
        out_data.delta_u_h[:,k:k+1] = delta_u_h
    
    return out_data


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%       Init filter          %%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def init_filter(settings):
    
    # Kalman filter state matrix
    P = np.zeros((15,15))
    P[0:3,0:3] = settings.factp[0]**2*np.eye(3)
    P[3:6,3:6] = settings.factp[1]**2*np.eye(3)
    P[6:9,6:9] = np.diag(settings.factp[2:5])**2 #FIX
    P[ 9:12, 9:12] = settings.factp[5]**2*np.eye(3)
    P[12:15,12:15] = settings.factp[6]**2*np.eye(3)
    
    # Process noise covariance
    Q1 = np.zeros((6,6))
    Q1[0:3,0:3] = np.diag([settings.sigma_acc])**2
    Q1[3:6,3:6] = np.diag([settings.sigma_gyro])**2
    
    Q2 = np.zeros((6,6))
    Q2[0:3,0:3] = settings.sigma_acc_bias**2*np.eye(3)
    Q2[3:6,3:6] = settings.sigma_gyro_bias**2*np.eye(3)
    
    # GNSS-receiver position measurement noise
    R = settings.sigma_gps**2*np.eye(3)
    
    # Observation matrix
    H = [np.eye(3), np.zeros((3,12))]
    
    return (P,Q1,Q2,R,H)


def init_navigation_state(u, settings):

    # Calculate the roll and pitch
    f = np.mean(u[:,0:100], axis=1)
    roll = np.arctan2(-f[1],-f[2]) # gy/gz
    pitch = np.arctan2(f[0], np.linalg.norm(f[1:3]))
    
    # Initial coordinate rotation matrix
    Rb2t = Rt2b([roll, pitch, settings.init_heading]).T #TODO
    
    # Calculate quaternions
    q = dcm2q(Rb2t)
    
    # Initial navigation state vector
    x_h = np.vstack((np.zeros((6,1)), q))
    
    return x_h



#  State transition matrix   %%
def state_space_model(x, u, Ts):
    # Convert quaternion to DCM
    Rb2t = q2dcm(x[6:10])
    
    # Transform measured force to force in
    # the tangent plane coordinate system.
    f_t = Rb2t@u[0:3].squeeze()
    St = np.array([
        [0, -f_t[2], f_t[1]],
        [f_t[2], 0, -f_t[0]],
        [-f_t[1], f_t[0], 0],
    ])
    
    # Only the standard errors included
    O = np.zeros((3,3))
    I = np.eye(3)
    # eq(13)
    Fc = np.block([
        [O, I,  O,    O,    O],
        [O, O, St, Rb2t,    O],
        [O, O,  O,    O,-Rb2t],
        [O, O,  O,    O,    O],
        [O, O,  O,    O,    O],
    ])
    
    # Approximation of the discret
    # time state transition matrix
    F = np.eye(15) + Ts*Fc
    
    # Noise gain matrix
    G = Ts*np.block([
         [O   ,     O, O, O],
         [Rb2t,     O, O, O],
         [O   , -Rb2t, O, O],
         [O   ,     O, I, O],
         [O   ,     O, O, I],
    ])

    return F, G


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%     Error correction of quaternion    %%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
def Gamma(q, epsilon): #eq(10)
    # Convert quaternion to DCM
    R = q2dcm(q)
    
    # Construct skew symetric matrx
    OMEGA = np.block([
        [          0, -epsilon[2],  epsilon[1]],
        [ epsilon[2],           0, -epsilon[0]],
        [-epsilon[1],  epsilon[0],           0],
        ])
    
    # Correct the DCM matrix
    R = (np.eye(3)-OMEGA)@R
    
    # Calculte the corrected quaternions
    q = dcm2q(R)
    return q
