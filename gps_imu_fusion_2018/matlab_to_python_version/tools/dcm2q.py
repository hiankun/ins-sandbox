import numpy as np
def dcm2q(R):
    # function q=dcm2q(R)
    # Function for transformation from directional cosine matrix to quaternions 
    # From Farrel pp 42.
    # Edit: Isaac Skog, 2007-05-24
    
    q = np.zeros((4,1))
    q[3] = 0.5*np.sqrt(1+np.sum(np.diag(R)))
    q[0] = (R[2,1]-R[1,2])/(4*q[3])
    q[1] = (R[0,2]-R[2,0])/(4*q[3])
    q[2] = (R[1,0]-R[0,1])/(4*q[3])
    
    return q
