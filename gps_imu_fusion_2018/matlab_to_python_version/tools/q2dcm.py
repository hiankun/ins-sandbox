import numpy as np

def q2dcm(q):
    # function R=q2dcm(q)
    # Function for transformation from quaternions to directional cosine matrix
    # Farell pp.41
    # Edit: Isaac Skog, 2007-05-24
    
    p = np.zeros((6,1))
    
    p[0:4] = q**2
    
    p[4] = p[1] + p[2]
    
    if (p[0]+p[3]+p[4] != 0):
        p[5] = 2/(p[0]+p[3]+p[4])
    else:
        p[5]=0
    
    R = np.zeros((3,3)) #TODO: check if R has been created outsides

    R[0,0] = 1-p[5]*p[4]
    R[1,1] = 1-p[5]*(p[0]+p[2])
    R[2,2] = 1-p[5]*(p[0]+p[1])
    
    p[0] = p[5]*q[0]
    p[1] = p[5]*q[1]
    p[4] = p[5]*q[2]*q[3]
    p[5] = p[0]*q[1]
    
    R[0,1] = p[5]-p[4]
    R[1,0] = p[5]+p[4]
    
    p[4] = p[1]*q[3]
    p[5] = p[0]*q[2]
    
    R[0,2] = p[5]+p[4]
    R[2,0] = p[5]-p[4]
    
    p[4] = p[0]*q[3]
    p[5] = p[1]*q[2]
    
    R[1,2] = p[5]-p[4]
    R[2,1] = p[5]+p[4]
    
    # R(1,2)=2*(q(1)*q(2)-q(3)*q(4))
    # R(1,3)=2*(q(1)*q(3)+q(2)*q(4))
    # 
    # % Row 2
    # R(2,1)=2*(q(1)*q(2)+q(3)*q(4))
    # R(2,2)=q(2)^2+q(4)^2-q(1)^2-q(3)^2
    # R(2,3)=2*(q(2)*q(3)-q(1)*q(4))
    # 
    # % Row 3
    # R(3,1)=2*(q(1)*q(3)-q(2)*q(4))
    # R(3,2)=2*(q(2)*q(3)+q(1)*q(4))
    # R(3,3)=q(3)^2+q(4)^2-q(1)^2-q(2)^2
    # 
    # % Okej, men kan förbätrras, se pp 41 Farrel
    
    return R
