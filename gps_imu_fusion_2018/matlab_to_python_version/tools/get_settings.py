from math import inf, pi, sqrt
import numpy as np
from tools.gravity import gravity as _gravity

class Settings:
    # GENERAL PARAMETERS
    gnss_outage = 'off'
    outagestart = 200
    outagestop = inf
    non_holonomic = 'off'
    speed_aiding = 'off'

    init_heading = 320*pi/180

    # FILTER PARAMETERS
    # Process noise covariance (Q)
    # Standard deviations, need to be squared
    sigma_acc = 0.05 #[m/s^2]
    sigma_gyro = 0.1*pi/180 #[rad/s]
    sigma_acc_bias = 0.0001 #[m/s^2.5]
    sigma_gyro_bias = 0.01*pi/180 #[rad/s^1.5]

    # GNSS position measurement noise covariance (R)
    # Standard deviations, need to be squared
    sigma_gps = 3/sqrt(3) #[m]
    sigma_speed = 1 #[m/s] Trim here
    #sigma_speed = 24 #[m/s] Trim here
    sigma_non_holonomic = 20 #[m/s] Trim here
    #sigma_non_holonomic = 3 #[m/s] Trim here

    # Initial Kalman filter uncertainties (standard deviations)
    factp = [
        10, #Position [m]
        5,  #Velocity [m/s]
        #TODO: transpose(); type/shape might be wrong
        pi/180*1, #Attitude (roll,pitch,yaw) [rad]
        pi/180*1,
        pi/180*20,
        0.02, #Accelerometer biases [m/s^2]
        pi/180*0.05, #Gyro biases [rad/s]
    ]

    gravity = _gravity(59, 0)
