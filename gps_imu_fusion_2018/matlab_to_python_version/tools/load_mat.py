import scipy.io
import sys
from pathlib import Path
import pandas as pd
from dotmap import DotMap


def load(mat_fname=None):
    if not mat_fname:
        mat_fname = sys.argv[1]
    out_fname = f'{Path(mat_fname).stem}.txt'
    
    data = scipy.io.loadmat(mat_fname)
    in_data = DotMap()

    #FIX: squeeze() maybe not needed...
    #NOTE: change squeeze() to [0][0]
    in_data.GNSS.pos_ned = \
            data['in_data']['GNSS'][0][0]['pos_ned'][0][0]
    in_data.GNSS.HDOP = \
            data['in_data']['GNSS'][0][0]['HDOP'][0][0]
    in_data.GNSS.VDOP = \
            data['in_data']['GNSS'][0][0]['VDOP'][0][0]
    in_data.GNSS.t = \
            data['in_data']['GNSS'][0][0]['t'][0][0]

    in_data.SPEEDOMETER.t = \
            data['in_data']['SPEEDOMETER'][0][0]['t'][0][0]
    in_data.SPEEDOMETER.speed = \
            data['in_data']['SPEEDOMETER'][0][0]['speed'][0][0]

    in_data.IMU.t = \
            data['in_data']['IMU'][0][0]['t'][0][0]
    in_data.IMU.acc = \
            data['in_data']['IMU'][0][0]['acc'][0][0]
    in_data.IMU.gyro = \
            data['in_data']['IMU'][0][0]['gyro'][0][0]
    
    return in_data

#    gnss_data = {}
#    gnss_data['pos_n'] = in_data['GNSS'][0][0][0][0][0][0]
#    gnss_data['pos_e'] = in_data['GNSS'][0][0][0][0][0][1]
#    gnss_data['pos_d'] = in_data['GNSS'][0][0][0][0][0][2]
#    gnss_data['HDOP'] = in_data['GNSS'][0][0][0][0][1].flatten()
#    gnss_data['VDOP'] = in_data['GNSS'][0][0][0][0][2].flatten()
#    gnss_data['t'] = in_data['GNSS'][0][0][0][0][3].flatten()
#    
#    speedo_data = {}
#    speedo_data['t'] = in_data['SPEEDOMETER'][0][0][0][0][0].flatten()
#    speedo_data['speed'] = in_data['SPEEDOMETER'][0][0][0][0][1].flatten()
#    
#    imu_data = {}
#    imu_data['t']= in_data['IMU'][0][0][0][0][0].flatten()
#    imu_data['acc_x']= in_data['IMU'][0][0][0][0][1][0]
#    imu_data['acc_y']= in_data['IMU'][0][0][0][0][1][1]
#    imu_data['acc_z']= in_data['IMU'][0][0][0][0][1][2]
#    imu_data['gyro_x']= in_data['IMU'][0][0][0][0][2][0]
#    imu_data['gyro_y']= in_data['IMU'][0][0][0][0][2][1]
#    imu_data['gyro_z']= in_data['IMU'][0][0][0][0][2][2]
#    
#    gnss = pd.DataFrame.from_dict(gnss_data)
#    speedo = pd.DataFrame.from_dict(speedo_data)
#    imu = pd.DataFrame.from_dict(imu_data)
#    #print(gnss.head(5))
#    #print(speedo.head(5))
#    #print(imu.head(5))
#
#    return gnss, speedo, imu

def main():
    load()


if __name__=='__main__':
    main()

