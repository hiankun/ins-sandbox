# Function that returns the directional cosine matrix that that relates the
# body (IMU coordinate system) to the platform (vehicle coordinate system)
# coordinate frame.
#
# Edit: Isaac Skog (skog@kth.se), 2016-09-08

import numpy as np

def get_Rb2p():
    Rb2p = np.array([
        [0.9880, -0.1472, -0.0463],
        [0.1540,  0.9605,  0.2319],
        [0.0103, -0.2363,  0.9716],
    ])
       
    return Rb2p
