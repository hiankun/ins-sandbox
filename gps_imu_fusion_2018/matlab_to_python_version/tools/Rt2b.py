import numpy as np

def Rt2b(ang):
    # function for calculation of the rotation matrix for
    # rotaion from tangent frame to body frame.
    # function R=Rt2b[roll,pitch,yaw];
    # v_b=[u v d]'
    # v_t=[N E D]'
    
    cr = np.cos(ang[0])
    sr = np.sin(ang[0])
    
    cp = np.cos(ang[1])
    sp = np.sin(ang[1])
    
    cy = np.cos(ang[2])
    sy = np.sin(ang[2])
    
    R = np.array([
            [cy*cp, sy*cp, -sp], 
            [-sy*cr+cy*sp*sr, cy*cr+sy*sp*sr, cp*sr], 
            [sy*sr+cy*sp*cr, -cy*sr+sy*sp*cr, cp*cr]
            ])
    
    return R
