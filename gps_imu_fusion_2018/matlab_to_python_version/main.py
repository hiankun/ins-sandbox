from tools.load_mat import load
from tools.get_settings import Settings
from GPSaidedINS import GPSaidedINS
import matplotlib.pyplot as plt
import pandas as pd

def plot_res(df_in, df_out, settings):
    fig, axs = plt.subplots(1,1, figsize=(9,9))

    axs.plot(df_in.pN, df_in.pE, 'b.-', label='GPS pos')
    axs.plot(df_out.px, df_out.py, 'r.-', label='estimated pos')
    axs.legend()

    gnss_outage = settings.gnss_outage
    if gnss_outage == 'on':
        gnss_outage_t0 = settings.outagestart
        gnss_outage_t1 = settings.outagestop
    else:
        gnss_outage_t0 = 0
        gnss_outage_t1 = 0
    non_holonomic = settings.non_holonomic
    speed_aiding = settings.speed_aiding
    title = (f'GNSS out: {gnss_outage_t0} to {gnss_outage_t1}\n'
             f'non holonomic: {settings.non_holonomic}\n'
             f'speed aiding: {settings.speed_aiding}'
            )
    axs.set_title(title)
    plt.show()

def main():
    print('Loads data')
    #gnss, speedo, imu = load()
    #odict_keys(['GNSS', 'SPEEDOMETER', 'IMU'])
    #GNSS: odict_keys(['pos_ned', 'HDOP', 'VDOP', 't'])
    #SPEEDOMETER: odict_keys(['t', 'speed'])
    #IMU: odict_keys(['t', 'acc', 'gyro'])
    in_data = load()
    df_in = pd.DataFrame(in_data.GNSS.pos_ned.T)
    df_in.columns = ['pN','pE','pD']
    
    print('Loads settings')
    settings = Settings()
    
    print('Runs the GNSS-aided INS')
    #out_data.keys():['x_h', 'diag_P', 'delta_u_h']
    out_data = GPSaidedINS(in_data, settings)
    df_out = pd.DataFrame(out_data.x_h.T)
    df_out.columns = ['px','py','pz','vx','vy','vz','q0','q1','q2','q3']
    #df.to_csv('out.csv')
    plot_res(df_in, df_out, settings)


if __name__=='__main__':
    main()
