import numpy as np
from tools.gravity import gravity
from Nav_eq import Nav_eq
import matplotlib.pyplot as plt

def plot_res(t, posS, posL, diff):
    fig, (ax1, ax2, ax3) = plt.subplots(3,1, figsize=(9,18))
    ax1.plot(t,posS[0],label='x')
    ax1.plot(t,posS[1],'r:', label='y')
    ax1.plot(t,posS[2],label='z')
    ax1.set_title('Stockholm: 59° 19′ 46″ nord')
    ax1.set_ylabel('positoin [m]')
    ax1.legend()

    ax2.plot(t,posL[0],label='x')
    ax2.plot(t,posL[1],'r:', label='y')
    ax2.plot(t,posL[2],label='z')
    ax2.set_title('Lund: 55° 42′ nord')
    ax2.set_ylabel('positoin [m]')
    ax2.legend()

    ax3.plot(t,diff[0],label='Δx')
    ax3.plot(t,diff[1],'r:', label='Δy')
    ax3.plot(t,diff[2],label='Δz')
    ax3.set_xlabel('time [s]')
    ax3.set_ylabel('diff (S - L) [m]')
    ax3.legend()

    plt.show()

#    figure(1)
#    clf
#    #plot(t,pos)
#    #plot(t,posL')
#    plot(t,posS)
#    #plot(t,diff)
#    grid on
#    ylabel('Position error [m]')
#    #ylabel('Difference in position error [m]')
#    xlabel('Time [s]')
#    legend('x-axis error','y-axis error','z-axis error')
#    
#    
#    figure(2)
#    #loglog(t,posS')#TODO: transpose()
#    pos2=9.82*gyrobias(1)*t.^3/6;  # theoretical 
#    pos3=9.82*(gyrobias(1))^2*t.^4/24; # theoretical
#    hold on
#    loglog(t,pos2,'k--')
#    loglog(t,pos3,'k--')

def main():
    Ts = 0.01
    Tmax = 60
    t = np.arange(0, Tmax, Ts)
    N = len(t)
    
    # Initialize state vector x
    xS = np.zeros((10,1))
    xS[-1] = 1
    xL = np.zeros((10,1))
    xL[-1] = 1
    
    # Try out different biases
    #Stockholm  59° 19′ 46″ nord
    #Lund 55° 42′ nord
    accbias = np.array([[0], [0], [0]])
    gyrobias = np.array([[0.01*np.pi/180], [0], [0]])
    #gyrobias = np.array([[0], [0], [0]])
    
    gS = gravity(59,0)
    uS = np.vstack((gS + accbias, gyrobias))
    gL = gravity(55,0)
    uL = np.vstack((gL + accbias, gyrobias))
    
    # simulate IMU standalone navigation system
    posS = np.zeros((3,N))
    posL = np.zeros((3,N))
    
    for n in range(1, N):
       xS = Nav_eq(xS,uS,Ts,gS)
       xL = Nav_eq(xL,uL,Ts,gL)
       posS[:,n:n+1] = xS[0:3]
       posL[:,n:n+1] = xL[0:3]
    
    diff = posS - posL
    #pos = np.vstack((posS, posL))
    plot_res(t, posS, posL, diff)
    

if __name__=='__main__':
    main()
