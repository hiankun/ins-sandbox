https://numpy.org/doc/stable/user/numpy-for-matlab-users.html

[Wed Aug 30 11:52:09 PM CST 2023]
* [DONE] TODO: `GPSaidedINS.py`, `out_data`

[Fri Sep  1 04:53:43 PM CST 2023]
* Complete the basic python version.
* Some test results:

  ![](worklog/pics/fig_0.png)
  ![](worklog/pics/fig_1.png)
  ![](worklog/pics/fig_2.png)
  ![](worklog/pics/fig_3.png)
