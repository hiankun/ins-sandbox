#set heading(numbering: "1.")
#show par: set block(spacing: 0.65em)
#set par(
  first-line-indent: 1em,
  justify: true,
)

This is a note on the document "SensorFusionGPSIMULocalisation.pdf",
which will be refered as "the document" in this note.

#let ub(v) = {$upright(bold(#v))$}
= Definition of vectors and matrices
$
x_k_(10,1) eq.delta vec(
    #ub[p]_k_(3,1),
    #ub[v]_k_(3,1),
    #ub[q]_k_(4,1))
$

$
u_k_(6,1) eq.delta vec(
    #ub[s]_k_(3,1),
    #ub[$omega$]_k_(3,1)
)
$

where
- $x_k$: navigation state 
- $u_k$: inertial measurement input

Here the $#ub[q]_k_(4,1)$ might cause confusion. 
In literatures I have already accessed,
the state matrices in KF/EKF are all in the shape of $(15,1)$ instead of $(16,1)$.
Also, in later sections of the document, it apparently use $(15,1)$ and $(15,15)$ vectors and matrix.

See the following section for more explanations (with some my guesses).

= Equations of update method

Equations (3) to (5) are used to update system states *in navigation frame*.
Note that as mentioned earlier, the quaternion 

In Chinese literatures they are "姿态更新", "速度更新" and "位置更新", respectively.
For example, in @rocket_redun, Equation (5) has the following form:
$
bold(q)_(b(k))^(b(k-1)) = cos(italic(Phi)_k/2) 
    + (bold(Phi)_k/italic(Phi)_k)sin(italic(Phi)_k/2)
$

== Code explanation
In `Nav_eq.py` we did the update of navigation states (pos, vel, att).

In `LN33` the attitude was transformed from quaternion to DCM,
the reason is (my guess) that only by doing so we can substract the gravity 
from the measurements of accelerometer,
which is in the form of $(3,1)$ vector in the navigation frame.
The process was illustrated in Figure 2 of the document (annotated in @fig2_nav_eq).

From LN63 to LN66 the attitude was updated in quaternion form.
One reason might be the numerical stability of quaternion which could avoid the gimble-lock problem.

A side note: there's an essential topic of "equivalent rotation matrix (等效旋转矩阵)",
which might be relevant in the attitude update implementation mentioned here.

```python
22  def Nav_eq(x,u,dt,g_t):

31      f_t = q2dcm(x[6:10])@u[0:3]
32      # Subtract gravity, to obtain accelerations in tangent plane coordinates
33      acc_t = f_t - g_t

44      # Position and velocity update
45      x[0:6] = A@x[0:6] + B@acc_t

63      v = np.linalg.norm(w_tb)*dt
64    
65      if v != 0:
66          x[6:10] = (np.cos(v/2)*np.eye(4)+2/v*np.sin(v/2)*OMEGA )@x[6:10]
```
#figure(
    image("pics/fig2_nav_eq.png"),
    caption: [Annotated version of Figure 2 in the document;
    note that this block was named "Navigation Equations" and was
    implemented in the `Nav_eq()` function.],
)<fig2_nav_eq>

In `GPSaidedINS.py` when we need to update the state matrix,
the attitude was transformed from quaternion to DCM to fit the state vector's shape $(15,1)$.

```python
216     def state_space_model(x, u, Ts):
217         # Convert quaternion to DCM
218         Rb2t = q2dcm(x[6:10])
```

= Remarks

So far, my understandings are:
+ When we need to "rotate" (updating attitude), use quaternion to avoid gimble-lock.
+ When we need to update all the states, use DCM which is easier to fit the navigation frame (e.g., NED).
+ All the state update should be done in the navigation frame or you'll lost the physical meanings of "navigation."

#bibliography("ref.bib")
