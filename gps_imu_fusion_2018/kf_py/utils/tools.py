import scipy.io
import sys
from pathlib import Path
import pandas as pd
from dotmap import DotMap

from math import inf, pi, sqrt, sin
import numpy as np

def gravity(lambda_, h):
    # function for calculation of the local gravity vector, in
    # the geographic reference frame (same as tangent plane is 
    # stationary).
    #
    # Based upon the WGS_84 Geodetic and Gravity model. For more 
    # info see [pp 222-223,1].
    #
    # lambda -> Latitude [degrees]
    # h -> Altitude [m]
    # g 
    #
    # edit: Isaac Skog, 2006-08-17
    # python: thk, 2023-08-23

    # degrees to radians
    lambda_=pi/180*lambda_;
    
    gamma = 9.780327*(1+0.0053024*sin(lambda_)**2
            -0.0000058*sin(2*lambda_)**2)
    
    g = (gamma - ((3.0877e-6)-(0.004e-6)*sin(lambda_)**2)*h
            + (0.072e-12)*h**2)
    
    return np.array([[0], [0], [-g]])

class Settings:
    # GENERAL PARAMETERS
    gnss_outage = 'off'
    outagestart = 200
    outagestop = inf
    non_holonomic = 'off'
    speed_aiding = 'off'

    init_heading = 320*pi/180

    # FILTER PARAMETERS
    # Process noise covariance (Q)
    # Standard deviations, need to be squared
    sigma_acc = 0.05 #[m/s^2]
    sigma_gyro = 0.1*pi/180 #[rad/s]
    sigma_acc_bias = 0.0001 #[m/s^2.5]
    sigma_gyro_bias = 0.01*pi/180 #[rad/s^1.5]

    # GNSS position measurement noise covariance (R)
    # Standard deviations, need to be squared
    sigma_gps = 3/sqrt(3) #[m]
    sigma_speed = 1 #[m/s] Trim here
    #sigma_speed = 24 #[m/s] Trim here
    sigma_non_holonomic = 20 #[m/s] Trim here
    #sigma_non_holonomic = 3 #[m/s] Trim here

    # Initial Kalman filter uncertainties (standard deviations)
    factp = [
        10, #Position [m]
        5,  #Velocity [m/s]
        #TODO: transpose(); type/shape might be wrong
        pi/180*1, #Attitude (roll,pitch,yaw) [rad]
        pi/180*1,
        pi/180*20,
        0.02, #Accelerometer biases [m/s^2]
        pi/180*0.05, #Gyro biases [rad/s]
    ]

    gravity = gravity(59, 0)

def load_mat(mat_fname=None):
    if not mat_fname:
        mat_fname = sys.argv[1]
    out_fname = f'{Path(mat_fname).stem}.txt'
    
    data = scipy.io.loadmat(mat_fname)
    in_data = DotMap()

    #FIX: squeeze() maybe not needed...
    #NOTE: change squeeze() to [0][0]
    in_data.GNSS.pos_ned = \
            data['in_data']['GNSS'][0][0]['pos_ned'][0][0]
    in_data.GNSS.HDOP = \
            data['in_data']['GNSS'][0][0]['HDOP'][0][0]
    in_data.GNSS.VDOP = \
            data['in_data']['GNSS'][0][0]['VDOP'][0][0]
    in_data.GNSS.t = \
            data['in_data']['GNSS'][0][0]['t'][0][0]

    in_data.SPEEDOMETER.t = \
            data['in_data']['SPEEDOMETER'][0][0]['t'][0][0]
    in_data.SPEEDOMETER.speed = \
            data['in_data']['SPEEDOMETER'][0][0]['speed'][0][0]

    in_data.IMU.t = \
            data['in_data']['IMU'][0][0]['t'][0][0]
    in_data.IMU.acc = \
            data['in_data']['IMU'][0][0]['acc'][0][0]
    in_data.IMU.gyro = \
            data['in_data']['IMU'][0][0]['gyro'][0][0]
    
    return in_data


def main():
    load_mat()


if __name__=='__main__':
    main()
