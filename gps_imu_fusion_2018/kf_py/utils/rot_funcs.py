# Function that returns the directional cosine matrix that that relates the
# body (IMU coordinate system) to the platform (vehicle coordinate system)
# coordinate frame.
#
# Edit: Isaac Skog (skog@kth.se), 2016-09-08

import numpy as np

def get_Rb2p():
    Rb2p = np.array([
        [0.9880, -0.1472, -0.0463],
        [0.1540,  0.9605,  0.2319],
        [0.0103, -0.2363,  0.9716],
    ])
       
    return Rb2p

def dcm2q(R):
    # Function for transformation from directional cosine matrix to quaternions 
    # From Farrel pp 42.
    # Edit: Isaac Skog, 2007-05-24
    
    q = np.zeros((4,1))
    q[3] = 0.5*np.sqrt(1+np.sum(np.diag(R)))
    q[0] = (R[2,1]-R[1,2])/(4*q[3])
    q[1] = (R[0,2]-R[2,0])/(4*q[3])
    q[2] = (R[1,0]-R[0,1])/(4*q[3])
    
    return q

def q2dcm(q):
    # Function for transformation from quaternions to directional cosine matrix
    # Farell pp.41
    # Edit: Isaac Skog, 2007-05-24
    
    p = np.zeros((6,1))
    
    p[0:4] = q**2
    
    p[4] = p[1] + p[2]
    
    if (p[0]+p[3]+p[4] != 0):
        p[5] = 2/(p[0]+p[3]+p[4])
    else:
        p[5]=0
    
    R = np.zeros((3,3)) #TODO: check if R has been created outsides

    R[0,0] = 1-p[5]*p[4]
    R[1,1] = 1-p[5]*(p[0]+p[2])
    R[2,2] = 1-p[5]*(p[0]+p[1])
    
    p[0] = p[5]*q[0]
    p[1] = p[5]*q[1]
    p[4] = p[5]*q[2]*q[3]
    p[5] = p[0]*q[1]
    
    R[0,1] = p[5]-p[4]
    R[1,0] = p[5]+p[4]
    
    p[4] = p[1]*q[3]
    p[5] = p[0]*q[2]
    
    R[0,2] = p[5]+p[4]
    R[2,0] = p[5]-p[4]
    
    p[4] = p[0]*q[3]
    p[5] = p[1]*q[2]
    
    R[1,2] = p[5]-p[4]
    R[2,1] = p[5]+p[4]
    
    # R(1,2)=2*(q(1)*q(2)-q(3)*q(4))
    # R(1,3)=2*(q(1)*q(3)+q(2)*q(4))
    # 
    # % Row 2
    # R(2,1)=2*(q(1)*q(2)+q(3)*q(4))
    # R(2,2)=q(2)^2+q(4)^2-q(1)^2-q(3)^2
    # R(2,3)=2*(q(2)*q(3)-q(1)*q(4))
    # 
    # % Row 3
    # R(3,1)=2*(q(1)*q(3)-q(2)*q(4))
    # R(3,2)=2*(q(2)*q(3)+q(1)*q(4))
    # R(3,3)=q(3)^2+q(4)^2-q(1)^2-q(2)^2
    # 
    # % Okej, men kan förbätrras, se pp 41 Farrel
    
    return R

def Rt2b(ang):
    # function for calculation of the rotation matrix for
    # rotaion from tangent frame to body frame.
    # function R=Rt2b[roll,pitch,yaw];
    # v_b=[u v d]'
    # v_t=[N E D]'
    
    cr = np.cos(ang[0])
    sr = np.sin(ang[0])
    
    cp = np.cos(ang[1])
    sp = np.sin(ang[1])
    
    cy = np.cos(ang[2])
    sy = np.sin(ang[2])
    
    R = np.array([
            [cy*cp, sy*cp, -sp], 
            [-sy*cr+cy*sp*sr, cy*cr+sy*sp*sr, cp*sr], 
            [sy*sr+cy*sp*cr, -cy*sr+sy*sp*cr, cp*cr]
            ])
    
    return R
