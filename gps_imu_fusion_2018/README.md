* source: https://github.com/nharrand/WASP_1_1

* `WASP_1_1-master/`: original repo
* `kf_py/`: I'm trying to convert the matlab code to python.
    * TASK #1: completed.
    * TASK #2: under construction.
    * TASK #3 and #4: ~~seems not so relevant and I have no plan to complete them.~~

[Fri Sep  1 06:25:57 PM CST 2023]
* Complete "1-to-1" conversion from Matlab to Python. (see `matlab_to_python_version/`)
* Start to refactor the Python code.

---
some useful links:

* [Estimation II, by Ian Reid (PDF file)](https://www.robots.ox.ac.uk/~ian/Teaching/Estimation/LectureNotes2.pdf): examples and code
  * [The course material link](https://www.robots.ox.ac.uk/~ian/Teaching/Estimation/)
* [The Kalman Filter: Derivation and Interpretation](https://nrotella.github.io/journal/kalman-filter-derivation-interpretation.html)
